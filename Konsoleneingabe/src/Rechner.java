import java.util.Scanner; // Import der Klasse Scanner

public class Rechner {

	public static void main(String[] args) // Hier startet das Programm
	{

		// Neues Scanner-Objekt myScanner wird erstellt
		Scanner myScanner = new Scanner(System.in);

		System.out.print("Bitte geben Sie eine ganze Zahl ein: ");

		// Die Variable zahl1 speichert die erste Eingabe
		double zahl1 = myScanner.nextDouble();

		System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");

		// Die Variable zahl2 speichert die zweite Eingabe
		double zahl2 = myScanner.nextDouble();

		// Die Addition der Variablen zahl1 und zahl2
		// wird der Variable ergebnis zugewiesen.
		double addition = zahl1 + zahl2;
		double subtraktion = zahl1 - zahl2;
		double multiplikation = zahl1 * zahl2;
		double division = zahl1 / zahl2;

		System.out.print("\nErgebnis der Addition lautet: ");
		System.out.print(zahl1 + " + " + zahl2 + " = " + addition);
		System.out.print("\nErgebnis der Subtraktion lautet: ");
		System.out.print(zahl1 + " - " + zahl2 + " = " + subtraktion);
		System.out.print("\nErgebnis der Multiplikation lautet: ");
		System.out.print(zahl1 + " * " + zahl2 + " = " + multiplikation);
		System.out.print("\nErgebnis der Division lautet: ");
		System.out.print(zahl1 + " / " + zahl2 + " = " + division);
	
		myScanner.close();
	}
}