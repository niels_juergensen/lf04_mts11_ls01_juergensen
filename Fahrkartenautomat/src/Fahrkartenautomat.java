import java.util.Scanner;

class Fahrkartenautomat {

    static Scanner tastatur = new Scanner(System.in);

    public static double fahrkartenbestellungErfassen() {
        System.out.println("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus:");
        System.out.println("\t Einzelfahrschein Regeltarif AB [2,90 EUR] (1)");
        System.out.println("\t Tageskarte Regeltarif AB [8,60 EUR] (2)");
        System.out.println("\t Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)");

        double ticketpreis = 0.0;

        do {
            String auswahl = tastatur.next();
            System.out.println("Ihre Wahl: " + auswahl);

            switch (auswahl) {
                case "1":
                    ticketpreis = 2.90;
                    break;
                case "2":
                    ticketpreis = 8.60;
                    break;
                case "3":
                    ticketpreis = 23.50;
                    break;
                default:
                    System.out.println(" >>falsche Eingabe<<");
                    break;
            }
        } while (ticketpreis == 0.0);

        System.out.print("Anzahl der Tickets: ");
        byte anzahlDerTickets = tastatur.nextByte();

        return ticketpreis * anzahlDerTickets;
    }

    public static double fahrkartenBezahlen(double zuZahlen) {
        double eingezahlterGesamtbetrag = 0.0;
        while (eingezahlterGesamtbetrag < zuZahlen) {
            System.out.printf("Noch zu zahlen: %.2f Euro\n", (zuZahlen - eingezahlterGesamtbetrag));
            System.out.print("Eingabe (mind. 5Ct, hoechstens 2,00 Euro): ");
            double eingeworfeneMuenze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneMuenze;
        }

        return eingezahlterGesamtbetrag - zuZahlen;
    }

    public static void fahrkartenAusgeben() {
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++) {
            System.out.print("=");
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        System.out.println("\n\n");
    }

    public static void rueckgeldAusgeben(double rueckgabebetrag) {
        if (rueckgabebetrag > 0.0) {
            System.out.printf("Der Rueckgabebetrag in Hoehe von %.2f Euro\n", rueckgabebetrag);
            System.out.println("wird in folgenden Muenzen ausgezahlt:");

            while (rueckgabebetrag >= 2.0) // 2 EURO-Muenzen
            {
                System.out.println("2,00 EURO");
                rueckgabebetrag -= 2.0;
            }
            while (rueckgabebetrag >= 1.0) // 1 EURO-Muenzen
            {
                System.out.println("1,00 EURO");
                rueckgabebetrag -= 1.0;
            }
            while (rueckgabebetrag >= 0.5) // 50 CENT-Muenzen
            {
                System.out.println("50 CENT");
                rueckgabebetrag -= 0.5;
            }
            while (rueckgabebetrag >= 0.2) // 20 CENT-Muenzen
            {
                System.out.println("20 CENT");
                rueckgabebetrag -= 0.2;
            }
            while (rueckgabebetrag >= 0.1) // 10 CENT-Muenzen
            {
                System.out.println("10 CENT");
                rueckgabebetrag -= 0.1;
            }
            while (rueckgabebetrag >= 0.05)// 5 CENT-Muenzen
            {
                System.out.println("5 CENT");
                rueckgabebetrag -= 0.05;
            }
        }
    }

    public static void main(String[] args) {
        while (true) {
            double zuZahlenderBetrag;
            double rueckgabebetrag;

            // Fahrkatenpreis und -anzahl aufnehmen und Gesamtpreis zurückgeben
            // ----------------------------------------------------------------
            zuZahlenderBetrag = fahrkartenbestellungErfassen();

            // Fahrkartenbezahlung mit Geldeinwurf und Rueckgeldberechnung
            // -----------------------------------------------------------
            rueckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);

            // Fahrscheinausgabe
            // -----------------
            fahrkartenAusgeben();

            // Rückgeld ausgeben
            // -----------------
            rueckgeldAusgeben(rueckgabebetrag);

            System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
                    + "Wir wuenschen Ihnen eine gute Fahrt. \n");
        }
    }
}