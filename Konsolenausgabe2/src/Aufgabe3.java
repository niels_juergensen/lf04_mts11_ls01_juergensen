
public class Aufgabe3 {

	public static void main(String[] args) {

		String linie = "---------------------------";

		String f1 = "-20";
		String f2 = "-10";
		String f3 = "+0";
		String f4 = "+20";
		String f5 = "+30";
		
		double c1 = -28.8889;
		double c2 = -23.3333;
		double c3 = -17.7778;
		double c4 = -6.6667;
		double c5 = -1.1111;


		//nur -11 weil der | das 12. Zeichen ist
		System.out.printf("%-11s|", "Fahrenheit");
		System.out.printf("%10s\n", "Celsius");

		System.out.printf("%-22.22s\n", linie);

		System.out.printf("%-11s|%10.2f\n", f1, c1);
		System.out.printf("%-11s|%10.2f\n", f2, c2);
		System.out.printf("%-11s|%10.2f\n", f3, c3);
		System.out.printf("%-11s|%10.2f\n", f4, c4);
		System.out.printf("%-11s|%10.2f\n", f5, c5);

	}

}
