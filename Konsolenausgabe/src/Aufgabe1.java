public class Aufgabe1 {

	public static void main(String[] args) {
		
		System.out.println("Die 1. Zeile von Niels Beispiel.");
		System.out.println("Die 2. Zeile von Niels Beispiel.");


		
		String name="Niels";
		
		int zeile1 = 1;
		int zeile2 = 2;
		
		System.out.println("Dieses Mal sind beide Zeilen in der selben Zeile:");
		System.out.print("Die "+zeile1+". Zeile von "+name+" Beispiel.");
		System.out.println(" Die "+zeile2+". Zeile von "+name+" Beispiel.");
		
	}

}
