import java.util.Scanner;

public class ForAufgabe8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int seitenlaengeQuadrat = scanner.nextInt();

        for(int i =0; i<seitenlaengeQuadrat;i++){
            System.out.print("* ");
            for(int j=0; j<seitenlaengeQuadrat-2; j++){
                if(i==0 || i==seitenlaengeQuadrat-1){
                    System.out.print("* ");
                }
                else {
                    System.out.print("  ");
                }
            }
            System.out.println("*");
        }
    }
}
