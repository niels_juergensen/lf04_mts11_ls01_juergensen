import java.util.Scanner;

public class ForAufgabe9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Treppenhöhe:");
        int treppenhoehe = scanner.nextInt();

        System.out.println("Treppenbreite:");
        int treppenbreite = scanner.nextInt();

        for (int i = 1; i <= treppenhoehe; i++) {
            for (int j = 1; j <= treppenhoehe; j++) {
                for (int k = 1; k <= treppenbreite; k++) {
                    if (i + j > treppenhoehe) {
                        System.out.print("*");
                    } else {
                        System.out.print(" ");
                    }
                }
            }
            System.out.println();
        }
    }
}
