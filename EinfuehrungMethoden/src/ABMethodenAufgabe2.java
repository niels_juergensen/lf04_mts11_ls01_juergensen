public class ABMethodenAufgabe2 {

	public static double multiplizieren(double factor1, double factor2) {
		return factor1 * factor2;
	}

	public static void main(String[] args) {
		System.out.println(multiplizieren(2.36, 7.87));
	}
}
